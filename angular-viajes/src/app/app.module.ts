import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViajesComponent } from './viajes/viajes.component';
import { DestinosComponent } from './destinos/destinos.component';

@NgModule({
  declarations: [
    AppComponent,
    ViajesComponent,
    DestinosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
