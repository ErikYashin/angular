import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destinos',
  templateUrl: './destinos.component.html',
  styleUrls: ['./destinos.component.css']
})
export class DestinosComponent implements OnInit {
  ciudad: DestinoViaje[];
  constructor() { 
  	this.ciudad = [];
  }

  ngOnInit() {
  }
  guardar(nombre:string, url:string):boolean {
  	this.destinos.push(new DestinoViaje(nombre, url));
  	
  	return false;
  }

}
