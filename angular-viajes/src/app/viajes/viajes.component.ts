import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-viajes',
  templateUrl: './viajes.component.html',
  styleUrls: ['./viajes.component.css']
})
export class ViajesComponent implements OnInit {
  @Input() destinos: DestinoViaje;
  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() {}

  ngOnInit() {
  }

}
